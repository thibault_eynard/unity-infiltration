using UnityEngine;
using System.Collections;

public class anime : MonoBehaviour {
	
	public string  anim1;
	public string  anim2;
	// Use this for initialization
	void Start () {
		this.animation.Play(anim1);	
		this.animation.Blend(anim2, 0.4F);
	}
}
