using UnityEngine;
using System.Collections;

public class move : MonoBehaviour {
	
	private NavMeshAgent agent;
	int i = 0;
	public float wait = 1;
	public Transform[] destination;

	
	void Start() 
	{
		this.agent = this.GetComponent<NavMeshAgent>();
		this.agent.SetDestination(this.destination[i].position);
	}
			
	void OnTriggerEnter(Collider other)
	{
		if (other.transform.GetInstanceID() != this.destination[i].GetInstanceID())
			return;	
		Invoke("Stop", this.wait);
	}
	
	void Stop()
	{	
		i++;
		if(i >= this.destination.Length)
			i = 0;
		this.agent.SetDestination(this.destination[i].position);	
	}
	
	void Update()
	{

	}
}
