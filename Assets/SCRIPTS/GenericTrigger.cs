using UnityEngine;
using System.Collections;

public class GenericTrigger : MonoBehaviour {

	public string message;
	
	void OnTriggerEnter(Collider other)
	{
		/// quand un objet entre dans le trigger, la méthode "message" est appele sur l'objet
		other.gameObject.SendMessage(this.message, SendMessageOptions.DontRequireReceiver);
	}
}
