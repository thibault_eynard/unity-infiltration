using UnityEngine;
using System.Collections;

public class randomColor : MonoBehaviour {

	public Color color1 = Color.blue;
    public Color color2 = Color.red;
    public float duration = 1.0F;
	
    void Update() {
        float t = Mathf.PingPong(Time.time, duration) / duration;
        this.renderer.material.color = Color.Lerp(color1, color2, t);
    }
}
