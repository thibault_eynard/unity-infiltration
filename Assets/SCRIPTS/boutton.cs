using UnityEngine;
using System.Collections;

public class boutton : MonoBehaviour {

	public GameObject pont;
	private Vector3 angle = new Vector3(0,0,-45);
	private bool myBool = true;
	
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnGUI()
	{
		if (GUI.Button (new Rect(Screen.width/2 - 40, 10, 80, 40), "BRIDGE"))
		{
			NavMeshAgent[] capsules = FindObjectsOfType(typeof(NavMeshAgent)) as NavMeshAgent[];
			if (myBool)
			{
				pont.transform.Rotate(angle);
				for (var i = 0; i < capsules.Length; i++)
				{					
					capsules[i].walkableMask = 7;					
				}
				myBool = false;
			}
			else
			{
				pont.transform.Rotate(-angle);
				for (var i = 0; i < capsules.Length; i++)
				{
					if(capsules[i].tag != "Gros")
						capsules[i].walkableMask = 15;
				}
				myBool = true;
			}
		}
	}
}
