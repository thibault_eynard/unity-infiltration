using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public string NextLevelName;
	
	void Win()
	{
		Application.LoadLevel(this.NextLevelName);	
	}
	
	void Die()
	{
		Application.LoadLevel(Application.loadedLevel);	
	}
}
