using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

	public GUISkin skin;
	public Color color1 = Color.red;
    public Color color2 = Color.blue;
    public float duration = 3.0F;
	
    void Update() {
        float t = Mathf.PingPong(Time.time, duration) / duration;
        camera.backgroundColor = Color.Lerp(color1, color2, t);
    }
	
	void OnGUI()
	{
		if (this.skin)
			GUI.skin = this.skin;
		if (GUI.Button (new Rect((Screen.width / 2) - 300, 200, 600, 100), "Mode normal"))
		{
			Application.LoadLevel ("level1");
		}
		if (GUI.Button (new Rect((Screen.width / 2) - 300, 400, 600, 100), "Mode time attack"))
		{
			Application.LoadLevel ("level1time");
		}
		if (GUI.Button (new Rect((Screen.width / 2) - 300, 600, 600, 100), "Quit"))
		{
			Application.Quit();
		}
	}
}
