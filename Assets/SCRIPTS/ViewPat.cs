using UnityEngine;
using System.Collections;

public class ViewPat : MonoBehaviour {
	
	public int range;
	public GameObject player;
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if (this.Alarme())
		{
			player.gameObject.SendMessage("Die", SendMessageOptions.DontRequireReceiver);
		}
		else
		{
			this.renderer.material.color = Color.red;
		}
	}
	
	bool Alarme()
	{
		Vector3 mPosition = this.transform.position;
		Vector3 pPosition = player.transform.position;
		
		float playerDistance = Vector3.Distance(pPosition, mPosition);
		
		if(playerDistance>this.range)
		{
			return false;
		}
		
		if(Vector3.Angle(pPosition - mPosition, this.transform.forward)>45)
		{
			return false;	
		}
		
		Ray ray = new Ray(mPosition ,pPosition - mPosition);
		Debug.DrawLine(ray.origin, ray.origin + pPosition - mPosition);
		RaycastHit info;
		
		if(Physics.Raycast(ray, out info, playerDistance))
		{
			if(info.transform.tag=="Player")
			{
				Debug.Log("i see !");
				return true;
			}
		}
		else
		{
			Debug.Log("i don't see");
		}
		
		return false;
		
		
		
	}
}
