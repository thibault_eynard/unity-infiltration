using UnityEngine;
using System.Collections;

public class showTime : MonoBehaviour {
	
	public float timeAllowed;
	private float timeLeft = 0;
	public GameObject player;

	void Start() {
		timeLeft = timeAllowed;
	}
	
	void Update () {
		timeLeft = timeAllowed - Time.timeSinceLevelLoad;
		guiText.text = "Timer = " + timeLeft.ToString("f1");
		if (timeLeft <= 0)
		{
			player.gameObject.SendMessage("Die", SendMessageOptions.DontRequireReceiver);
		}
	}
}
